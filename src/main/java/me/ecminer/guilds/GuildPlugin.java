package me.ecminer.guilds;

import me.ecminer.guilds.chat.Chat;
import me.ecminer.guilds.chat.Messages;
import me.ecminer.guilds.command.commands.CommandConfirm;
import me.ecminer.guilds.command.commands.CommandG;
import me.ecminer.guilds.command.commands.CommandGuild;
import me.ecminer.guilds.database.Database;
import me.ecminer.guilds.database.implementations.MySQL;
import me.ecminer.guilds.guild.Guild;
import me.ecminer.guilds.guild.GuildManager;
import me.ecminer.guilds.guild.GuildPlayer;
import me.ecminer.guilds.listener.PlayerListener;
import me.ecminer.guilds.packet.PacketHandler;
import me.ecminer.guilds.packet.PacketManager;
import me.ecminer.guilds.packet.packets.*;
import me.ecminer.guilds.synchronization.Async;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class GuildPlugin extends JavaPlugin {

    private static GuildPlugin instance;

    public static GuildPlugin getInstance() {
        return instance;
    }

    private GuildManager guildManager;
    private Async async;
    private Database database;
    private Chat chat;
    private PacketManager packetManager;

    public GuildManager getGuildManager() {
        return guildManager;
    }

    public Async getAsync() {
        return async;
    }

    public Database getDataBase() {
        return database;
    }

    public Chat getChat() {
        return chat;
    }

    public PacketManager getPacketManager() {
        return packetManager;
    }

    @Override
    public void onEnable() {
        instance = this;

        saveDefaultConfig();
        saveResource("chat.yml", false);

        guildManager = new GuildManager();
        async = new Async();
        database = new MySQL(getConfig().getString("MySQL.host", "127.0.0.1"), getConfig().getInt("MySQL.port", 3306), getConfig().getString("MySQL.username", "user"), getConfig().getString("MySQL.password", "pwd"), getConfig().getString("MySQL.database", "guilds"));
        database.prepareDatabase();
        chat = new Chat(YamlConfiguration.loadConfiguration(new File(getDataFolder(), "chat.yml")));
        packetManager = new PacketManager();

        registerListeners();
        loadCommands();
        registerPackets();
        registerHandlers();
    }

    @Override
    public void onDisable() {
        instance = null;

        guildManager.unload();
        guildManager = null;

        async.unload();
        async = null;

        database.unload();
        database = null;

        chat.unload();
        chat = null;

        packetManager.unload();
        packetManager = null;
    }

    private void registerListeners() {
        PluginManager pm = getServer().getPluginManager();

        pm.registerEvents(new PlayerListener(), this);
    }

    private void loadCommands() {
        new CommandGuild();
        new CommandConfirm();
        new CommandG();
    }

    private void registerPackets() {
        packetManager.registerPacket(PacketGuildDeleted.class);
        packetManager.registerPacket(PacketMemberJoin.class);
        packetManager.registerPacket(PacketMemberKick.class);
        packetManager.registerPacket(PacketMemberLeave.class);
        packetManager.registerPacket(PacketUpdateRank.class);
        packetManager.registerPacket(PacketOwnerChanged.class);
    }

    private void registerHandlers() {
        packetManager.addPacketHandler(PacketGuildDeleted.class, new PacketHandler<PacketGuildDeleted>() {

            @Override
            public void onPacketReceived(PacketGuildDeleted packet) {
                if (guildManager.isGuildLoaded(packet.getGuildName())) {
                    Guild guild = guildManager.getGuild(packet.getGuildName());
                    for (GuildPlayer member : guild.getOnlinePlayers()) {
                        GuildPlugin.getInstance().getChat().sendError(member.getPlayer(), Messages.Error.OWNER_DELETED_GUILD);
                    }
                    guildManager.unloadGuild(guild);
                }
            }

        });
        packetManager.addPacketHandler(PacketMemberJoin.class, new PacketHandler<PacketMemberJoin>() {

            @Override
            public void onPacketReceived(PacketMemberJoin packet) {
                if (guildManager.isGuildLoaded(packet.getGuildName())) {
                    Guild guild = guildManager.getGuild(packet.getGuildName());
                    for (GuildPlayer member : guild.getOnlinePlayers()) {
                        GuildPlugin.getInstance().getChat().sendNormal(member.getPlayer(), Messages.Normal.MEMBER_JOINED, packet.getPlayerName());
                    }

                    guild.addMember(packet.getPlayerId(), packet.getPlayerName(), false);
                }
            }

        });
        packetManager.addPacketHandler(PacketMemberKick.class, new PacketHandler<PacketMemberKick>() {

            @Override
            public void onPacketReceived(PacketMemberKick packet) {
                if (guildManager.isGuildLoaded(packet.getGuildName())) {
                    Guild guild = guildManager.getGuild(packet.getGuildName());
                    guild.removeMember(packet.getPlayerId(), false);

                    for (GuildPlayer member : guild.getOnlinePlayers()) {
                        GuildPlugin.getInstance().getChat().sendNormal(member.getPlayer(), Messages.Normal.PLAYER_KICKED, packet.getPlayerName(), packet.getKickedBy());
                    }
                }
            }

        });
        packetManager.addPacketHandler(PacketMemberLeave.class, new PacketHandler<PacketMemberLeave>() {

            @Override
            public void onPacketReceived(PacketMemberLeave packet) {
                if (guildManager.isGuildLoaded(packet.getGuildName())) {
                    Guild guild = guildManager.getGuild(packet.getGuildName());

                    guild.removeMember(packet.getPlayerId(), false);

                    for (GuildPlayer online : guild.getOnlinePlayers()) {
                        GuildPlugin.getInstance().getChat().sendError(online.getPlayer(), Messages.Error.LEFT_GUILD, packet.getPlayerName());
                    }
                }
            }

        });
        packetManager.addPacketHandler(PacketUpdateRank.class, new PacketHandler<PacketUpdateRank>() {

            @Override
            public void onPacketReceived(PacketUpdateRank packet) {
                if (guildManager.isGuildLoaded(packet.getGuildName())) {
                    Guild guild = guildManager.getGuild(packet.getGuildName());

                    guild.getMember(packet.getPlayerId()).setRank(packet.getGuildRank(), false);

                    if (packet.getChangedBy() != null) {
                        Player player = Bukkit.getPlayer(packet.getPlayerId());
                        if (player != null) {
                            chat.sendNormal(player, Messages.Normal.YOUR_RANK_CHANGED, packet.getChangedBy(), packet.getGuildRank().getPrettyName());
                        }
                    }
                }
            }

        });
        packetManager.addPacketHandler(PacketOwnerChanged.class, new PacketHandler<PacketOwnerChanged>() {

            @Override
            public void onPacketReceived(PacketOwnerChanged packet) {
                if (guildManager.isGuildLoaded(packet.getGuildName())) {
                    Guild guild = guildManager.getGuild(packet.getGuildName());

                    guild.setOwner(packet.getNewOwner(), false);

                    Player newOwner = Bukkit.getPlayer(packet.getNewOwner());
                    if (newOwner != null) {
                        GuildPlugin.getInstance().getChat().sendNormal(newOwner, Messages.Normal.NEW_OWNER);
                    }
                    for (GuildPlayer online : guild.getOnlinePlayers()) {
                        if (!online.getPlayerId().toString().equals(packet.getNewOwner().toString())) {
                            GuildPlugin.getInstance().getChat().sendNormal(online.getPlayer(), Messages.Normal.OWNER_CHANGED, packet.getNewOwnerName());
                        }
                    }
                    // TODO Send message.
                }
            }

        });
    }
}
