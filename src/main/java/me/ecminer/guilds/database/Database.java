package me.ecminer.guilds.database;

import me.ecminer.guilds.guild.Guild;
import me.ecminer.guilds.guild.GuildPlayer;
import me.ecminer.guilds.guild.GuildRank;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public interface Database {

    // TODO Write prototype for necessary database methods
    void prepareDatabase();

    void deleteGuild(Guild guild);

    void deleteGuild(String guildName);

    void createGuild(Guild guild);

    void createGuild(String guildName, GuildPlayer owner);

    void addMember(GuildPlayer guildPlayer);

    void removeMember(Player player);

    void removeMember(UUID playerId);

    void banMember(Guild guild, Player player);

    void banMember(String guildName, Player player);

    void banMember(Guild guild, UUID playerId);

    void banMember(String guildName, UUID playerId);

    Guild loadGuild(String guildName);

    List<String> loadAlliedGuilds(String guildName);

    List<String> loadWarredGuilds(String guildName);

    List<UUID> loadBannedPlayers(String guildName);

    List<GuildPlayer> loadMembers(String guildName);

    String getRealGuildName(String guildName);

    void removeWarredGuild(Guild guild, Guild warredGuild);

    void removeWarredGuild(String guildName, Guild warredGuild);

    void removeWarredGuild(Guild guild, String warredGuild);

    void removeWarredGuild(String guildName, String warredGuild);

    void addWarredGuild(Guild guild, Guild warredGuild);

    void addWarredGuild(String guildName, Guild warredGuild);

    void addWarredGuild(Guild guild, String warredGuild);

    void addWarredGuild(String guildName, String warredGuild);

    void removeAlliedGuild(Guild guild, Guild alliedGuild);

    void removeAlliedGuild(String guildName, Guild alliedGuild);

    void removeAlliedGuild(Guild guild, String alliedGuild);

    void removeAlliedGuild(String guildName, String alliedGuild);

    void addAlliedGuild(Guild guild, Guild alliedGuild);

    void addAlliedGuild(String guildName, Guild alliedGuild);

    void addAlliedGuild(Guild guild, String alliedGuild);

    void addAlliedGuild(String guildName, String alliedGuild);

    void unBanPlayer(Guild guild, Player player);

    void unBanPlayer(Guild guild, UUID playerId);

    void unBanPlayer(String guildName, Player player);

    void unBanPlayer(String guildName, UUID playerId);

    void banPlayer(Guild guild, Player player);

    void banPlayer(Guild guild, UUID playerId);

    void banPlayer(String guildName, Player player);

    void banPlayer(String guildName, UUID playerId);

    String getPlayerGuild(Player player);

    String getPlayerGuild(UUID playerId);

    boolean guildExists(String guildName);

    void setOwner(Guild guild, Player player);

    void setOwner(String guildName, Player player);

    void setOwner(Guild guild, UUID owner);

    void setOwner(String guildName, UUID owner);

    void setRank(Player player, GuildRank rank);

    void setRank(UUID playerId, GuildRank rank);

    void updateName(UUID playerId, String name);

    UUID getIdFromName(String name);

    String getNameFromId(UUID playerId);

    GuildRank getRank(Player player);

    GuildRank getRank(UUID playerId);

    void unload();

}
