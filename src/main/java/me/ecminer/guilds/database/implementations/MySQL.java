package me.ecminer.guilds.database.implementations;

import me.ecminer.guilds.database.Database;
import me.ecminer.guilds.guild.Guild;
import me.ecminer.guilds.guild.GuildPlayer;
import me.ecminer.guilds.guild.GuildRank;
import org.bukkit.entity.Player;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MySQL implements Database {

    private Connection connection;
    private final String databaseName;

    private final String guildsTable = "guilds";
    private final String guildPlayersTable = "guild_players";
    private final String guildsWarredTable = "guilds_warred";
    private final String guildsAlliedTable = "guilds_allied";
    private final String guildsBansTable = "guild_bans";

    public MySQL(String host, int port, String username, String password, String databaseName) {
        this.databaseName = databaseName;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port, username, password);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private int execute(String query, Object... values) {
        try {
            PreparedStatement statement = connection.prepareStatement(query);

            for (int i = 0; i < values.length; i++) {
                Object obj = values[i];
                statement.setObject(i + 1, obj);
            }

            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private ResultSet query(String query, Object... values) {
        try {
            PreparedStatement statement = connection.prepareStatement(query);

            for (int i = 0; i < values.length; i++) {
                Object obj = values[i];
                statement.setObject(i + 1, obj);
            }

            return statement.executeQuery();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void prepareDatabase() {
        execute("CREATE DATABASE IF NOT EXISTS " + databaseName + ";");
        execute("USE " + databaseName + ";");
        execute("CREATE TABLE IF NOT EXISTS " + guildsTable + "(name VARCHAR(255) NOT NULL, owner CHAR(36) NOT NULL, PRIMARY KEY(name));");
        execute("CREATE TABLE IF NOT EXISTS " + guildPlayersTable + "(uuid CHAR(36) NOT NULL, guild VARCHAR(255) NOT NULL, rank VARCHAR(40) NOT NULL, name VARCHAR(40), PRIMARY KEY (uuid), FOREIGN KEY (guild) REFERENCES guilds(name));");
        execute("CREATE TABLE IF NOT EXISTS " + guildsWarredTable + "(guild VARCHAR(255) NOT NULL, warred_guild VARCHAR(255) NOT NULL, PRIMARY KEY (guild, warred_guild), INDEX IDX_guildswarred_1 (warred_guild), FOREIGN KEY (guild) REFERENCES guilds(name), FOREIGN KEY (warred_guild) REFERENCES guilds(name));");
        execute("CREATE TABLE IF NOT EXISTS " + guildsAlliedTable + "(guild VARCHAR(255) NOT NULL, allied_guild VARCHAR(255) NOT NULL, PRIMARY KEY (guild, allied_guild), INDEX IDX_guildsallied_1 (allied_guild), FOREIGN KEY (guild) REFERENCES guilds(name), FOREIGN KEY (allied_guild) REFERENCES guilds(name));");
        execute("CREATE TABLE IF NOT EXISTS " + guildsBansTable + "(guild VARCHAR(255) NOT NULL, banned_player CHAR(36) NOT NULL, PRIMARY KEY(guild, banned_player), INDEX IDX_guildbans_1 (banned_player), FOREIGN KEY(guild) REFERENCES guilds(name));");
    }

    @Override
    public void deleteGuild(Guild guild) {
        deleteGuild(guild.getName());
    }

    @Override
    public void deleteGuild(String guildName) {
        execute("DELETE FROM " + guildPlayersTable + " WHERE guild=?;", guildName);
        execute("DELETE FROM " + guildsWarredTable + " WHERE guild=? OR warred_guild=?;", guildName, guildName);
        execute("DELETE FROM " + guildsAlliedTable + " WHERE guild=? OR allied_guild=?;", guildName, guildName);
        execute("DELETE FROM " + guildsTable + " WHERE name=?;", guildName);
    }

    @Override
    public void createGuild(Guild guild) {
        createGuild(guild.getName(), guild.getOwner());
    }

    @Override
    public void createGuild(String guildName, GuildPlayer owner) {
        execute("INSERT IGNORE INTO " + guildsTable + "(name, owner) VALUES(?, ?);", guildName, owner.getPlayerId().toString());
        addMember(owner);
    }

    @Override
    public void addMember(GuildPlayer guildPlayer) {
        System.out.println(guildPlayer.getPlayerId().toString());
        System.out.println(guildPlayer.getGuildName());
        System.out.println(guildPlayer.getRank().name());
        System.out.println(guildPlayer.getName());
        execute("INSERT IGNORE INTO " + guildPlayersTable + "(uuid, guild, rank, name) VALUES(?, ?, ?, ?);", guildPlayer.getPlayerId().toString(), guildPlayer.getGuildName(), guildPlayer.getRank().name(), guildPlayer.getName());
    }

    @Override
    public void removeMember(Player player) {
        removeMember(player.getUniqueId());
    }

    @Override
    public void removeMember(UUID playerId) {
        execute("DELETE FROM " + guildPlayersTable + " WHERE uuid=?;", playerId.toString());
    }

    @Override
    public void banMember(Guild guild, Player player) {
        banMember(guild.getName(), player.getUniqueId());
    }

    @Override
    public void banMember(String guildName, Player player) {
        banMember(guildName, player.getUniqueId());
    }

    @Override
    public void banMember(Guild guild, UUID playerId) {
        banMember(guild.getName(), playerId);
    }

    @Override
    public void banMember(String guildName, UUID playerId) {
        execute("DELETE FROM " + guildPlayersTable + " WHERE uuid=? AND guild=?;", playerId.toString(), guildName);
        execute("INSERT IGNORE INTO " + guildsBansTable + "(guild, banned_player)) VALUES(?, ?)", guildName, playerId.toString());
    }

    @Override
    public Guild loadGuild(String guildName) {
        ResultSet result = query("SELECT * FROM " + guildsTable + " WHERE name=?;", guildName);
        try {
            if (result.next()) {
                guildName = result.getString("name");

                GuildPlayer owner = new GuildPlayer(UUID.fromString(result.getString("owner")), guildName);
                owner.setRank(GuildRank.OWNER);

                Guild guild = new Guild(guildName, owner);

                for (GuildPlayer member : loadMembers(guildName)) {
                    guild.getMembers().put(member.getPlayerId(), member);
                }

                guild.getAlliedGuilds().addAll(loadAlliedGuilds(guildName));
                guild.getWarredGuilds().addAll(loadWarredGuilds(guildName));
                guild.getBannedPlayers().addAll(loadBannedPlayers(guildName));

                return guild;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> loadAlliedGuilds(String guildName) {
        List<String> alliedGuilds = new ArrayList<>();

        ResultSet result = query("SELECT allied_guild FROM " + guildsAlliedTable + " WHERE guild=?;", guildName);
        try {
            while (result.next()) {
                alliedGuilds.add(result.getString("allied_guild"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return alliedGuilds;
    }

    @Override
    public List<String> loadWarredGuilds(String guildName) {
        List<String> warredGuilds = new ArrayList<>();

        ResultSet result = query("SELECT warred_guild FROM " + guildsWarredTable + " WHERE guild=?;", guildName);
        try {
            while (result.next()) {
                warredGuilds.add(result.getString("warred_guild"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return warredGuilds;
    }

    @Override
    public List<UUID> loadBannedPlayers(String guildName) {
        List<UUID> bannedPlayers = new ArrayList<>();

        ResultSet result = query("SELECT banned_player FROM " + guildsBansTable + " WHERE guild=?;", guildName);
        try {
            while (result.next()) {
                bannedPlayers.add(UUID.fromString(result.getString("banned_player")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return bannedPlayers;
    }

    @Override
    public List<GuildPlayer> loadMembers(String guildName) {
        List<GuildPlayer> members = new ArrayList<>();

        ResultSet result = query("SELECT uuid, rank, name FROM " + guildPlayersTable + " WHERE guild=?;", guildName);
        try {
            while (result.next()) {
                GuildPlayer guildPlayer = new GuildPlayer(UUID.fromString(result.getString("uuid")), guildName).setName(result.getString("name"));
                guildPlayer.setRank(GuildRank.valueOf(result.getString("rank")));
                members.add(guildPlayer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return members;
    }

    @Override
    public String getRealGuildName(String guildName) {
        ResultSet result = query("SELECT name FROM " + guildsTable + " WHERE name=?;", guildName);
        try {
            if (result.next()) {
                return result.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void removeWarredGuild(Guild guild, Guild warredGuild) {
        removeWarredGuild(guild.getName(), warredGuild.getName());
    }

    @Override
    public void removeWarredGuild(String guildName, Guild warredGuild) {
        removeWarredGuild(guildName, warredGuild.getName());
    }

    @Override
    public void removeWarredGuild(Guild guild, String warredGuild) {
        removeWarredGuild(guild.getName(), warredGuild);
    }

    @Override
    public void removeWarredGuild(String guildName, String warredGuild) {
        execute("DELETE FROM " + guildsWarredTable + " WHERE guild=? AND warred_guild=?;", guildName, warredGuild);
    }

    @Override
    public void addWarredGuild(Guild guild, Guild warredGuild) {
        addWarredGuild(guild.getName(), warredGuild.getName());
    }

    @Override
    public void addWarredGuild(String guildName, Guild warredGuild) {
        addWarredGuild(guildName, warredGuild.getName());
    }

    @Override
    public void addWarredGuild(Guild guild, String warredGuild) {
        addWarredGuild(guild.getName(), warredGuild);
    }

    @Override
    public void addWarredGuild(String guildName, String warredGuild) {
        execute("INSERT IGNORE INTO " + guildsWarredTable + "(guild, warred_guild) VALUES(?, ?);", guildName, warredGuild);
    }

    @Override
    public void removeAlliedGuild(Guild guild, Guild alliedGuild) {
        removeAlliedGuild(guild.getName(), alliedGuild.getName());
    }

    @Override
    public void removeAlliedGuild(String guildName, Guild alliedGuild) {
        removeAlliedGuild(guildName, alliedGuild.getName());
    }

    @Override
    public void removeAlliedGuild(Guild guild, String alliedGuild) {
        removeAlliedGuild(guild.getName(), alliedGuild);
    }

    @Override
    public void removeAlliedGuild(String guildName, String alliedGuild) {
        execute("DELETE FROM " + guildsAlliedTable + " WHERE guild=? AND allied_guild=?;", guildName, alliedGuild);
    }

    @Override
    public void addAlliedGuild(Guild guild, Guild alliedGuild) {
        addAlliedGuild(guild.getName(), alliedGuild);
    }

    @Override
    public void addAlliedGuild(String guildName, Guild alliedGuild) {
        addAlliedGuild(guildName, alliedGuild.getName());
    }

    @Override
    public void addAlliedGuild(Guild guild, String alliedGuild) {
        addAlliedGuild(guild.getName(), alliedGuild);
    }

    @Override
    public void addAlliedGuild(String guildName, String alliedGuild) {
        execute("INSERT IGNORE INTO " + guildsAlliedTable + "(guild, allied_guild) VALUES(?, ?);", guildName, alliedGuild);
    }

    @Override
    public void unBanPlayer(Guild guild, Player player) {
        unBanPlayer(guild.getName(), player.getUniqueId());
    }

    @Override
    public void unBanPlayer(Guild guild, UUID playerId) {
        unBanPlayer(guild.getName(), playerId);
    }

    @Override
    public void unBanPlayer(String guildName, Player player) {
        unBanPlayer(guildName, player.getUniqueId());
    }

    @Override
    public void unBanPlayer(String guildName, UUID playerId) {
        execute("DELETE FROM " + guildsBansTable + " WHERE guild=? AND banned_player=?;", guildName, playerId.toString());
    }

    @Override
    public void banPlayer(Guild guild, Player player) {
        banPlayer(guild.getName(), player.getUniqueId());
    }

    @Override
    public void banPlayer(Guild guild, UUID playerId) {
        banPlayer(guild.getName(), playerId);
    }

    @Override
    public void banPlayer(String guildName, Player player) {
        banPlayer(guildName, player.getUniqueId());
    }

    @Override
    public void banPlayer(String guildName, UUID playerId) {
        execute("INSERT IGNORE INTO " + guildsBansTable + "(guild, banned_player) VALUES(?, ?);", guildName, playerId.toString());
    }

    @Override
    public String getPlayerGuild(Player player) {
        return getPlayerGuild(player.getUniqueId());
    }

    @Override
    public String getPlayerGuild(UUID playerId) {
        ResultSet result = query("SELECT guild FROM " + guildPlayersTable + " WHERE uuid=?;", playerId.toString());

        try {
            if (result.next()) {
                return result.getString("guild");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean guildExists(String guildName) {
        ResultSet result = query("SELECT name FROM " + guildsTable + " WHERE name=?;", guildName);

        try {
            return result.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void setOwner(Guild guild, Player player) {
        setOwner(guild.getName(), player.getUniqueId());
    }

    @Override
    public void setOwner(String guildName, Player player) {
        setOwner(guildName, player.getUniqueId());
    }

    @Override
    public void setOwner(Guild guild, UUID owner) {
        setOwner(guild.getName(), owner);
    }

    @Override
    public void setOwner(String guildName, UUID owner) {
        execute("UPDATE " + guildsTable + " SET owner=? WHERE name=?;", owner.toString(), guildName);
        execute("UPDATE " + guildPlayersTable + " SET rank=? WHERE rank=?;", GuildRank.OWNER.name(), GuildRank.OWNER.name());
        execute("UPDATE " + guildPlayersTable + " SET rank=? WHERE uuid=?;", GuildRank.OWNER.name(), owner.toString());
    }

    @Override
    public void setRank(Player player, GuildRank rank) {
        setRank(player.getUniqueId(), rank);
    }

    @Override
    public void setRank(UUID playerId, GuildRank rank) {
        execute("UPDATE " + guildPlayersTable + " SET rank=? WHERE uuid=?;", rank.name(), playerId.toString());
    }

    @Override
    public void updateName(UUID playerId, String name) {
        execute("UPDATE " + guildPlayersTable + " SET name=NULL WHERE name=?;", name);
        execute("UPDATE " + guildPlayersTable + " SET name=? WHERE uuid=?;", name, playerId.toString());
    }

    @Override
    public UUID getIdFromName(String name) {
        ResultSet result = query("SELECT uuid FROM " + guildPlayersTable + " WHERE name=?;", name);
        try {
            if (result.next()) {
                return UUID.fromString(result.getString("uuid"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getNameFromId(UUID playerId) {
        ResultSet result = query("SELECT name FROM " + guildPlayersTable + " WHERE uuid=?;", playerId.toString());
        try {
            if (result.next()) {
                return result.getString("name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public GuildRank getRank(Player player) {
        return getRank(player.getUniqueId());
    }

    @Override
    public GuildRank getRank(UUID playerId) {
        ResultSet result = query("SELECT rank FROM " + guildPlayersTable + " WHERE uuid=?;", playerId.toString());

        try {
            if (result.next()) {
                return GuildRank.valueOf(result.getString("rank"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void unload() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connection = null;
    }
}
