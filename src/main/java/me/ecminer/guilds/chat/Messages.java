package me.ecminer.guilds.chat;

public class Messages {

    public enum Error {

        PLAYER_COMMAND("player-command"),
        CONSOLE_COMMAND("console-command"),
        NO_PERMISSION("no-permission"),
        GUILD_EXISTS("guild-exists"),
        IN_GUILD("in-guild"),
        DISABLED_CHARACTERS("disabled-characters"),
        NAME_TOO_LONG("name-too-long"),
        NOT_IN_GUILD("not-in-guild"),
        LEFT_GUILD("left-guild"),
        NEED_OWNER_RANK("need-owner-rank"),
        OWNER_DELETED_GUILD("owner-deleted-guild"),
        NO_CONFIRM_ACTION("no-confirm-actions"),
        NOT_ONLINE("not-online"),
        GUILD_RANK("guild-rank"),
        NO_INVITATION("no-invitation"),
        GUILD_NOT_EXISTS("guild-not-exists"),
        NOT_SAME_GUILD("not-same-guild"),
        HIGHER_RANK("higher-rank"),
        ACTION_ON_YOURSELF("action-on-yourself"),
        ALREADY_IN_GUILD("already-in-guild"),
        ALREADY_INVITED("already-invited"),
        RANK_DOESNT_EXIST("rank-doesnt-exist"),
        GIVEN_RANK_TOO_HIGH("given-rank-too-high");

        private final String configNode;

        Error(String configNode) {
            this.configNode = configNode.startsWith("messages.") ? configNode : "messages.error." + configNode;
        }

        public String getConfigNode() {
            return configNode;
        }
    }

    public enum Normal {

        GUILD_CREATED("guild-created"),
        LEFT_GUILD("left-guild"),
        OWNER_CHANGED("owner-changed"),
        NEW_OWNER("new-owner"),
        DELETED_GUILD("deleted-guild"),
        NEEDS_CONFIRMATION("needs-confirmation"),
        INVITED_PLAYER("invited-player"),
        INVITATION_RECEIVED("invitation-received"),
        MEMBER_JOINED("member-joined"),
        JOINED_GUILD("joined-guild"),
        PLAYER_KICKED("player-kicked"),
        KICKED_PLAYER("kicked-player"),
        KICKED("kicked"),
        YOUR_RANK_CHANGED("your-rank-changed"),
        CHANGED_RANK("changed-rank");

        private final String configNode;

        Normal(String configNode) {
            this.configNode = configNode.startsWith("messages.") ? configNode : "messages.normal." + configNode;
        }

        public String getConfigNode() {
            return configNode;
        }
    }

}
