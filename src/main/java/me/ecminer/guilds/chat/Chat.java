package me.ecminer.guilds.chat;

import me.ecminer.guilds.GuildPlugin;
import me.ecminer.guilds.collections.CaseInsensitiveMap;
import me.ecminer.guilds.guild.Guild;
import me.ecminer.guilds.guild.GuildPlayer;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class Chat {

    private final String normalPrefix;
    private final String normalDefaultColor;
    private final String normalMarkupColor;

    private final String errorPrefix;
    private final String errorDefaultColor;
    private final String errorMarkupColor;

    private final String guildChatFormat;

    private Map<String, String> messages = new CaseInsensitiveMap<>();

    public Chat(FileConfiguration config) {
        this.normalPrefix = ChatColor.translateAlternateColorCodes('&', config.getString("normal.prefix", ""));
        this.normalMarkupColor = ChatColor.translateAlternateColorCodes('&', config.getString("normal.markup-color", ""));
        this.normalDefaultColor = ChatColor.translateAlternateColorCodes('&', config.getString("normal.default-color", ""));

        this.errorPrefix = ChatColor.translateAlternateColorCodes('&', config.getString("error.prefix", ""));
        this.errorDefaultColor = ChatColor.translateAlternateColorCodes('&', config.getString("error.default-color", ""));
        this.errorMarkupColor = ChatColor.translateAlternateColorCodes('&', config.getString("error.markup-color", ""));

        this.guildChatFormat = ChatColor.translateAlternateColorCodes('&', config.getString("guild.format", ""));

        messages.putAll(getMessages("messages", config));
    }

    public void sendGuildChatMessage(Player player, String message) {
        if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
            Guild guild = GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player);
            for (GuildPlayer member : guild.getOnlinePlayers()) {
                member.getPlayer().sendMessage(guildChatFormat.replace("{%name}", player.getName()).replace("{%message}", message));
            }
        }
    }

    private Map<String, String> getMessages(String dir, FileConfiguration config) {
        Map<String, String> map = new HashMap<>();
        if (config.isSet(dir) && config.isConfigurationSection(dir)) {
            for (String key : config.getConfigurationSection(dir).getKeys(false)) {
                if (config.isConfigurationSection(dir + "." + key)) {
                    map.putAll(getMessages(dir + "." + key, config));
                } else {
                    map.put(dir + "." + key, config.getString(dir + "." + key));
                }
            }
        }
        return map;
    }

    private boolean messageExists(Messages.Error error) {
        return messageExists(error.getConfigNode());
    }

    private boolean messageExists(Messages.Normal normal) {
        return messageExists(normal.getConfigNode());
    }

    private boolean messageExists(String key) {
        return messages.containsKey(key);
    }

    public String getMessage(Messages.Error error) {
        return getMessage(error.getConfigNode());
    }

    public String getMessage(Messages.Normal normal) {
        return getMessage(normal.getConfigNode());
    }

    public String getMessage(String key) {
        return messages.get(key);
    }

    public void sendNormal(CommandSender sender, String key, Object... parameters) {
        sendMessage(sender, normalPrefix, normalDefaultColor, normalMarkupColor, key, parameters);
    }

    public void sendNormal(CommandSender sender, Messages.Normal message, Object... parameters) {
        sendMessage(sender, normalPrefix, normalDefaultColor, normalMarkupColor, message.getConfigNode(), parameters);
    }

    public void sendError(CommandSender sender, String key, Object... parameters) {
        sendMessage(sender, errorPrefix, errorDefaultColor, errorMarkupColor, key, parameters);
    }

    public void sendError(CommandSender sender, Messages.Error message, Object... parameters) {
        sendMessage(sender, errorPrefix, errorDefaultColor, errorMarkupColor, message.getConfigNode(), parameters);
    }

    public void sendMessage(CommandSender sender, String prefix, String defaultColor, String markupColor, String key, Object... parameters) {
        if (messageExists(key)) {
            String message = prefix + " " + defaultColor + ChatColor.translateAlternateColorCodes('&', getMessage(key)).replace("{%m}", markupColor).replace("{%n}", defaultColor);

            for (int i = 0; i < parameters.length; i++) {
                message = message.replace("{%" + (i + 1) + "}", parameters[i].toString());
            }

            sender.sendMessage(message);
        }
    }

    public void sendNormalToAll(String key, Object... parameters) {
        sendMessageToAll(normalPrefix, normalDefaultColor, normalMarkupColor, key, parameters);
    }

    public void sendNormalToAll(Messages.Normal message, Object... parameters) {
        sendMessageToAll(normalPrefix, normalDefaultColor, normalMarkupColor, message.getConfigNode(), parameters);
    }

    public void sendErrorToAll(String key, Object... parameters) {
        sendMessageToAll(errorPrefix, errorDefaultColor, errorMarkupColor, key, parameters);
    }

    public void sendErrorToAll(Messages.Error message, Object... parameters) {
        sendMessageToAll(errorPrefix, errorDefaultColor, errorMarkupColor, message.getConfigNode(), parameters);
    }

    public void sendMessageToAll(String prefix, String defaultColor, String markupColor, String key, Object... parameters) {
        if (messageExists(key)) {
            String message = prefix + " " + defaultColor + ChatColor.translateAlternateColorCodes('&', getMessage(key)).replace("{&m}", markupColor).replace("{%n}", defaultColor);

            for (int i = 0; i < parameters.length; i++) {
                message = message.replace("{%" + (i + 1) + "}", parameters[i].toString());
            }

            for (Player player : Bukkit.getOnlinePlayers()) {
                player.sendMessage(message);
            }
        }
    }

    public void unload() {
        messages.clear();
        messages = null;
    }
}
