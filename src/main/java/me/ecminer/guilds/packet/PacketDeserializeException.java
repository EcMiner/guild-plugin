package me.ecminer.guilds.packet;

public class PacketDeserializeException extends Exception {

    public PacketDeserializeException(String message) {
        super("Exception while de-serializing packets: " + message);
    }
}
