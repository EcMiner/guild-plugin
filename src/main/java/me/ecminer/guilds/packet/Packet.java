package me.ecminer.guilds.packet;

import me.ecminer.guilds.GuildPlugin;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.lang.reflect.InvocationTargetException;

public abstract class Packet {

    private static final Gson gson = new Gson();

    public abstract PacketSide getPacketSide();

    public final JsonObject serialize() {
        JsonObject obj = new JsonObject();
        obj.addProperty("packets", getClass().getSimpleName());

        JsonObject data = (JsonObject) gson.toJsonTree(this);
        obj.add("data", data);

        return obj;
    }

    public static Packet deserialize(JsonObject obj) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, PacketDeserializeException, NoSuchFieldException {
        if (obj.has("packets")) {
            Class<? extends Packet> packetClass = GuildPlugin.getInstance().getPacketManager().getRegisteredPacket(obj.get("packets").getAsString());
            if (obj.has("data")) {
                JsonObject data = obj.get("data").getAsJsonObject();
                return gson.fromJson(data, packetClass);
            } else {
                throw new PacketDeserializeException("The packets doesn't have any packets data!");
            }
        } else {
            throw new PacketDeserializeException("The packets doesn't have a packets name");
        }
    }
}
