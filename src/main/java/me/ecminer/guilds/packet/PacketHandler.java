package me.ecminer.guilds.packet;

public abstract class PacketHandler<P extends Packet> {

    public abstract void onPacketReceived(P packet);

    protected final void received(Packet packet) {
        onPacketReceived((P) packet);
    }

}
