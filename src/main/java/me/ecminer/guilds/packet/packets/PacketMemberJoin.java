package me.ecminer.guilds.packet.packets;

import me.ecminer.guilds.packet.Packet;
import me.ecminer.guilds.packet.PacketSide;

import java.util.UUID;

public class PacketMemberJoin extends Packet {

    private String guildName;
    private UUID playerId;
    private String playerName;

    public PacketMemberJoin(String guildName, UUID playerId, String playerName) {
        this.guildName = guildName;
        this.playerId = playerId;
        this.playerName = playerName;
    }

    public String getGuildName() {
        return guildName;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    @Override
    public PacketSide getPacketSide() {
        return PacketSide.BOTH;
    }
}
