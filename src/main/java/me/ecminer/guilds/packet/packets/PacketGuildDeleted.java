package me.ecminer.guilds.packet.packets;

import me.ecminer.guilds.packet.Packet;
import me.ecminer.guilds.packet.PacketSide;

public class PacketGuildDeleted extends Packet {

    private String guildName;

    public PacketGuildDeleted(String guildName) {
        this.guildName = guildName;
    }

    public String getGuildName() {
        return guildName;
    }

    @Override
    public PacketSide getPacketSide() {
        return PacketSide.BOTH;
    }
}
