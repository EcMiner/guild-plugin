package me.ecminer.guilds.packet.packets;

import me.ecminer.guilds.packet.Packet;
import me.ecminer.guilds.packet.PacketSide;

import java.util.UUID;

public class PacketMemberKick extends Packet {

    private String guildName;
    private UUID playerId;
    private String playerName;
    private String kickedBy;

    public PacketMemberKick(String guildName, UUID playerId, String playerName, String kickedBy) {
        this.guildName = guildName;
        this.playerId = playerId;
        this.playerName = playerName;
        this.kickedBy = kickedBy;
    }

    public String getGuildName() {
        return guildName;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getKickedBy() {
        return kickedBy;
    }

    @Override
    public PacketSide getPacketSide() {
        return PacketSide.BOTH;
    }
}
