package me.ecminer.guilds.packet.packets;

import me.ecminer.guilds.guild.GuildRank;
import me.ecminer.guilds.packet.Packet;
import me.ecminer.guilds.packet.PacketSide;

import java.util.UUID;

public class PacketUpdateRank extends Packet {

    private String guildName;
    private UUID playerId;
    private String playerName;
    private GuildRank guildRank;
    private String changedBy;

    public PacketUpdateRank(String guildName, UUID playerId, String playerName, GuildRank guildRank, String changedBy) {
        this.guildName = guildName;
        this.playerId = playerId;
        this.playerName = playerName;
        this.guildRank = guildRank;
        this.changedBy = changedBy;

    }

    public String getGuildName() {
        return guildName;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public GuildRank getGuildRank() {
        return guildRank;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getChangedBy() {
        return changedBy;
    }

    @Override
    public PacketSide getPacketSide() {
        return PacketSide.BOTH;
    }
}
