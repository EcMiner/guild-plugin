package me.ecminer.guilds.packet.packets;

import me.ecminer.guilds.packet.Packet;
import me.ecminer.guilds.packet.PacketSide;

import java.util.UUID;

public class PacketOwnerChanged extends Packet {

    private String guildName;
    private UUID newOwner;
    private String newOwnerName;

    public PacketOwnerChanged(String guildName, UUID newOwner, String newOwnerName) {
        this.guildName = guildName;
        this.newOwner = newOwner;
        this.newOwnerName = newOwnerName;
    }

    public String getGuildName() {
        return guildName;
    }

    public UUID getNewOwner() {
        return newOwner;
    }

    public String getNewOwnerName() {
        return newOwnerName;
    }

    @Override
    public PacketSide getPacketSide() {
        return PacketSide.BOTH;
    }
}
