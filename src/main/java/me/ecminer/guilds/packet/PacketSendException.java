package me.ecminer.guilds.packet;

public class PacketSendException extends Exception {

    public PacketSendException(String message) {
        super("Error while sending packets: " + message);
    }
}
