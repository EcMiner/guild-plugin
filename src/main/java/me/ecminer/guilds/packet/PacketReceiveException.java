package me.ecminer.guilds.packet;

public class PacketReceiveException extends Exception {

    public PacketReceiveException(String message) {
        super("Error while receiving packets: " + message);
    }
}
