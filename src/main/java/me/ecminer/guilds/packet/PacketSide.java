package me.ecminer.guilds.packet;

public enum PacketSide {

    /**
     * PacketSide Bungee means that only the BungeeCord proxy can send this kind of packets
     */
    BUNGEE,
    /**
     * PacketSide Spigot means that only a spigot server can send this kind of packets
     */
    SPIGOT,
    /**
     * PacketSide Both means that both the BungeeCord proxy and Spigot server can send this kind of packets
     */
    BOTH;

}
