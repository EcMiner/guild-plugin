package me.ecminer.guilds.packet;

import me.ecminer.guilds.GuildPlugin;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PacketManager implements PluginMessageListener {

    public static final String CHANNEL = "GuildsChannel";

    private Map<String, Class<? extends Packet>> registeredPackets = new HashMap<>();
    private Map<String, List<PacketHandler<?>>> packetHandlers = new HashMap<>();
    private final JsonParser parser = new JsonParser();

    public PacketManager() {
        GuildPlugin.getInstance().getServer().getMessenger().registerOutgoingPluginChannel(GuildPlugin.getInstance(), CHANNEL);
        GuildPlugin.getInstance().getServer().getMessenger().registerIncomingPluginChannel(GuildPlugin.getInstance(), CHANNEL, this);
    }

    public void registerPacket(Class<? extends Packet> packetClass) {
        registeredPackets.put(packetClass.getSimpleName().toLowerCase(), packetClass);
    }

    public boolean isPacketRegistered(String packetName) {
        return registeredPackets.containsKey(packetName.toLowerCase());
    }

    public Class<? extends Packet> getRegisteredPacket(String packetName) {
        return registeredPackets.get(packetName.toLowerCase());
    }

    public <P extends Packet> void addPacketHandler(Class<P> packet, PacketHandler<P> packetHandler) {
        if (!packetHandlers.containsKey(packet.getSimpleName().toLowerCase())) {
            packetHandlers.put(packet.getSimpleName().toLowerCase(), new ArrayList<>());
        }
        packetHandlers.get(packet.getSimpleName().toLowerCase()).add(packetHandler);
    }

    public boolean hasPacketHandlers(Class<? extends Packet> packetClass) {
        return packetHandlers.containsKey(packetClass.getSimpleName().toLowerCase());
    }

    @SuppressWarnings("unchecked")
    public <P extends Packet> List<PacketHandler<P>> getPacketHandlers(Class<P> packetClass) {
        List<PacketHandler<P>> list = new ArrayList<>();

        if (hasPacketHandlers(packetClass)) {
            for (PacketHandler<?> packetHandler : packetHandlers.get(packetClass.getSimpleName().toLowerCase())) {
                try {
                    list.add((PacketHandler<P>) packetHandler);
                } catch (Exception ex) {
                    System.err.println("Registered packetHandler doesn't have the same type!");
                }
            }
        }

        return list;
    }

    public void unload() {
        registeredPackets.clear();
        registeredPackets = null;

        packetHandlers.clear();
        packetHandlers = null;
    }

    public void sendPacket(Packet packet) {
        try {
            if (Bukkit.getOnlinePlayers().size() > 0) {
                if (packet.getPacketSide() != PacketSide.BUNGEE) {
                    if (isPacketRegistered(packet.getClass().getSimpleName())) {
                        ByteArrayDataOutput output = ByteStreams.newDataOutput();
                        output.writeUTF(packet.serialize().toString());

                        Bukkit.getOnlinePlayers().iterator().next().sendPluginMessage(GuildPlugin.getInstance(), CHANNEL, output.toByteArray());
                    } else {
                        throw new PacketSendException("Tried to send packets that isn't registered: " + packet.getClass().getSimpleName());
                    }
                } else {
                    throw new PacketSendException("Tried to send packets that can only be sent by a BungeeCord proxy: " + packet.getClass().getSimpleName());
                }
            } else {
                throw new PacketSendException("Can't send packets without any players online: " + packet.getClass().getSimpleName());
            }
        } catch (PacketSendException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] bytes) {
        if (!channel.equals(CHANNEL)) {
            return;
        }
        ByteArrayDataInput input = ByteStreams.newDataInput(bytes);
        String packet = input.readUTF();

        try {
            JsonObject obj = parser.parse(packet).getAsJsonObject();
            if (obj.has("packets")) {
                if (isPacketRegistered(obj.get("packets").getAsString())) {
                    if (obj.has("data")) {
                        try {
                            Packet packetObject = Packet.deserialize(obj);
                            if (packetObject.getPacketSide() != PacketSide.SPIGOT) {
                                for (PacketHandler<? extends Packet> packetHandler : getPacketHandlers(packetObject.getClass())) {
                                    packetHandler.received(packetObject);
                                }
                            } else {
                                throw new PacketReceiveException("Received illegal packets, can only be sent by a spigot server, not a BungeeCord proxy");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        throw new PacketReceiveException("Received packets doesn't have any packets data: " + packet);
                    }
                } else {
                    throw new PacketReceiveException("Received an unregistered packets: " + packet);
                }
            } else {
                throw new PacketReceiveException("Received packets doesn't have a packets name: " + packet);
            }
        } catch (JsonParseException | IllegalStateException ex) {
            try {
                throw new PacketReceiveException("Not a correct json packets: " + packet);
            } catch (PacketReceiveException e) {
                e.printStackTrace();
            }
        } catch (PacketReceiveException ex) {
            ex.printStackTrace();
        }
    }
}
