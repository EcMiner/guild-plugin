package me.ecminer.guilds.command.commands;

import me.ecminer.guilds.GuildPlugin;
import me.ecminer.guilds.chat.Messages;
import me.ecminer.guilds.command.ConfirmAction;
import me.ecminer.guilds.command.PlayerCommand;
import me.ecminer.guilds.guild.Guild;
import me.ecminer.guilds.guild.GuildPlayer;
import me.ecminer.guilds.guild.GuildRank;
import me.ecminer.guilds.packet.packets.*;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

public class CommandGuild extends PlayerCommand implements TabCompleter {

    public CommandGuild() {
        super("guild");
    }

    @Override
    public void onPlayerExecuted(Player player, String label, String[] args) {
        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("create")) {
                if (args.length == 2) {
                    if (!GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
                        String guildName = args[1];
                        if (guildName.length() <= Guild.maxNameLength) {
                            if (guildName.matches(Guild.allowedCharacters)) {
                                if (GuildPlugin.getInstance().getGuildManager().isGuildLoaded(guildName) || GuildPlugin.getInstance().getAsync().get(new Callable<Boolean>() {

                                    @Override
                                    public Boolean call() throws Exception {
                                        return GuildPlugin.getInstance().getDataBase().guildExists(guildName);
                                    }

                                })) {
                                    GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.GUILD_EXISTS, guildName);
                                } else {
                                    Guild guild = GuildPlugin.getInstance().getGuildManager().createGuild(guildName, player);
                                    GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.GUILD_CREATED, guild.getName());
                                }
                            } else {
                                GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.DISABLED_CHARACTERS, guildName);
                            }
                        } else {
                            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NAME_TOO_LONG, guildName, Guild.maxNameLength);
                        }
                    } else {
                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.IN_GUILD, label);
                    }
                } else {
                    // TODO Show help
                }
            } else if (args[0].equalsIgnoreCase("leave")) {
                if (args.length == 1) {
                    if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
                        new ConfirmAction(player) {

                            @Override
                            public void onConfirm(Player player) {
                                Guild guild = GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player);
                                GuildPlayer guildPlayer = guild.getMember(player);

                                if (guildPlayer.getRank() == GuildRank.OWNER) {
                                    if (guild.getMemberCount() > 1) {
                                        GuildPlayer newOwner = null;

                                        for (GuildPlayer member : guild.getMembers().values()) {
                                            if (newOwner == null || member.getRank().getRankLadder() > newOwner.getRank().getRankLadder()) {
                                                newOwner = member;
                                            }
                                        }

                                        if (newOwner != null) {
                                            for (GuildPlayer online : guild.getOnlinePlayers()) {
                                                if (online != guildPlayer && online != newOwner) {
                                                    GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.OWNER_CHANGED, newOwner.getName());
                                                }
                                            }
                                            if (newOwner.isOnline()) {
                                                GuildPlugin.getInstance().getChat().sendNormal(newOwner.getPlayer(), Messages.Normal.NEW_OWNER);
                                            }
                                            guild.setOwner(newOwner.getPlayerId(), true);
                                            GuildPlugin.getInstance().getPacketManager().sendPacket(new PacketOwnerChanged(guild.getName(), newOwner.getPlayerId(), newOwner.getName()));
                                        }
                                    }
                                }
                                for (GuildPlayer online : guild.getOnlinePlayers()) {
                                    if (online != guildPlayer) {
                                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.LEFT_GUILD, player.getName());
                                    }
                                }
                                guild.removeMember(player);
                                GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.LEFT_GUILD, guild.getName());
                                GuildPlugin.getInstance().getPacketManager().sendPacket(new PacketMemberLeave(guild.getName(), player.getUniqueId(), player.getName()));
                            }

                        };
                    } else {
                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_IN_GUILD);
                    }
                } else {
                    // TODO Show help
                }
            } else if (args[0].equalsIgnoreCase("delete")) {
                if (args.length == 1) {
                    if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
                        Guild guild = GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player);
                        GuildPlayer guildPlayer = guild.getMember(player);

                        if (guildPlayer.getRank() == GuildRank.OWNER) {
                            new ConfirmAction(player) {

                                @Override
                                public void onConfirm(Player player) {
                                    for (GuildPlayer member : guild.getOnlinePlayers()) {
                                        if (member != guildPlayer) {
                                            GuildPlugin.getInstance().getChat().sendError(member.getPlayer(), Messages.Error.OWNER_DELETED_GUILD);
                                        }
                                    }
                                    GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.DELETED_GUILD, guild.getName());
                                    GuildPlugin.getInstance().getGuildManager().deleteGuild(guild);
                                    GuildPlugin.getInstance().getPacketManager().sendPacket(new PacketGuildDeleted(guild.getName()));
                                }

                            };
                        } else {
                            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NEED_OWNER_RANK, guild.getName());
                        }
                    } else {
                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_IN_GUILD);
                    }
                } else {
                    // TODO Show help
                }
            } else if (args[0].equalsIgnoreCase("invite")) {
                if (args.length == 2) {
                    if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
                        Guild guild = GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player);
                        GuildPlayer guildPlayer = guild.getMember(player);

                        if (guildPlayer.getRank().isHigherOrEquals(GuildRank.OFFICER)) {
                            Player target = Bukkit.getPlayer(args[1]);
                            if (target != player) {
                                if (!GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(target) || GuildPlugin.getInstance().getGuildManager().getGuildFromMember(target) != guild) {
                                    if (target != null) {
                                        if (!GuildPlugin.getInstance().getGuildManager().hasInvitation(target, guild.getName())) {
                                            GuildPlugin.getInstance().getGuildManager().addInvitation(target, guild);
                                            GuildPlugin.getInstance().getChat().sendNormal(target, Messages.Normal.INVITATION_RECEIVED, guild.getName());
                                            GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.INVITED_PLAYER, target.getName());
                                        } else {
                                            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.ALREADY_INVITED, target.getName());
                                        }
                                    } else {
                                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_ONLINE, args[1]);
                                    }
                                } else {
                                    GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.ALREADY_IN_GUILD, target.getName());
                                }
                            } else {
                                GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.ACTION_ON_YOURSELF);
                            }
                        } else {
                            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.GUILD_RANK, GuildRank.OFFICER.getPrettyName());
                        }
                    } else {
                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_IN_GUILD);
                    }
                } else {
                    // TODO Show help
                }
            } else if (args[0].equalsIgnoreCase("accept")) {
                if (args.length == 2) {
                    if (!GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
                        String guildName = args[1];
                        if (GuildPlugin.getInstance().getGuildManager().hasInvitation(player, guildName)) {
                            if (GuildPlugin.getInstance().getGuildManager().isGuildLoaded(guildName)) {
                                Guild guild = GuildPlugin.getInstance().getGuildManager().getGuild(guildName);
                                for (GuildPlayer member : guild.getOnlinePlayers()) {
                                    GuildPlugin.getInstance().getChat().sendNormal(member.getPlayer(), Messages.Normal.MEMBER_JOINED, player.getName());
                                }
                                GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.JOINED_GUILD, guild.getName());
                                GuildPlugin.getInstance().getGuildManager().removeInvitations(player);
                                guild.addMember(player);
                                GuildPlugin.getInstance().getPacketManager().sendPacket(new PacketMemberJoin(guild.getName(), player.getUniqueId(), player.getDisplayName()));
                            } else {
                                Guild guild = GuildPlugin.getInstance().getGuildManager().loadGuild(guildName, false);
                                if (guild != null) {
                                    guild.addMember(player);
                                    GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.JOINED_GUILD, guild.getName());
                                    GuildPlugin.getInstance().getPacketManager().sendPacket(new PacketMemberJoin(guild.getName(), player.getUniqueId(), player.getDisplayName()));
                                } else {
                                    GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.GUILD_NOT_EXISTS, guildName);
                                }
                            }
                        } else {
                            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NO_INVITATION, guildName);
                        }
                    } else {
                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.IN_GUILD, label);
                    }
                } else {
                    // TODO Show help
                }
            } else if (args[0].equalsIgnoreCase("kick")) {
                if (args.length == 2) {
                    if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
                        Guild guild = GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player);
                        GuildPlayer guildPlayer = guild.getMember(player);
                        if (guildPlayer.getRank().isHigherOrEquals(GuildRank.LEADER)) {
                            Player target = Bukkit.getPlayer(args[1]);
                            if (target != null) {
                                if (target != player) {
                                    if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(target) && GuildPlugin.getInstance().getGuildManager().getGuildFromMember(target).equals(guild)) {
                                        GuildPlayer targetPlayer = guild.getMember(target);
                                        if (guildPlayer.getRank().isHigher(targetPlayer.getRank())) {
                                            guild.removeMember(target);
                                            for (GuildPlayer member : guild.getOnlinePlayers()) {
                                                if (member != guildPlayer) {
                                                    GuildPlugin.getInstance().getChat().sendNormal(member.getPlayer(), Messages.Normal.PLAYER_KICKED, target.getName(), player.getDisplayName());
                                                }
                                            }
                                            GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.KICKED_PLAYER, target.getName());
                                            GuildPlugin.getInstance().getChat().sendNormal(target, Messages.Normal.KICKED, guild.getName(), player.getName());
                                            GuildPlugin.getInstance().getPacketManager().sendPacket(new PacketMemberKick(guild.getName(), targetPlayer.getPlayerId(), targetPlayer.getName(), player.getName()));
                                        } else {
                                            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.HIGHER_RANK, target.getName());
                                        }
                                    } else {
                                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_SAME_GUILD, target.getName());
                                    }
                                } else {
                                    GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.ACTION_ON_YOURSELF);
                                }
                            } else {
                                UUID targetId = GuildPlugin.getInstance().getAsync().get(new Callable<UUID>() {
                                    @Override
                                    public UUID call() throws Exception {
                                        return GuildPlugin.getInstance().getDataBase().getIdFromName(args[1]);
                                    }
                                });

                                if (targetId != null && GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(targetId) && GuildPlugin.getInstance().getGuildManager().getGuildFromMember(targetId).equals(guild)) {
                                    GuildPlayer targetPlayer = guild.getMember(targetId);
                                    if (guildPlayer.getRank().isHigher(targetPlayer.getRank())) {
                                        guild.removeMember(targetPlayer.getPlayerId());
                                        for (GuildPlayer member : guild.getOnlinePlayers()) {
                                            if (member != guildPlayer) {
                                                GuildPlugin.getInstance().getChat().sendNormal(member.getPlayer(), Messages.Normal.PLAYER_KICKED, args[1], player.getDisplayName());
                                            }
                                        }
                                        GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.KICKED_PLAYER, args[1]);
                                        GuildPlugin.getInstance().getPacketManager().sendPacket(new PacketMemberKick(guild.getName(), targetPlayer.getPlayerId(), targetPlayer.getName(), player.getName()));
                                    } else {
                                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.HIGHER_RANK, args[1]);
                                    }
                                } else {
                                    GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_SAME_GUILD, args[1]);
                                }
                            }
                        } else {
                            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.GUILD_RANK, GuildRank.LEADER.getPrettyName());
                        }
                    } else {
                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_IN_GUILD);
                    }
                } else {
                    // TODO Show help
                }
            } else if (args[0].equalsIgnoreCase("setrank")) {
                if (args.length == 3) {
                    if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
                        Guild guild = GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player);
                        GuildPlayer guildPlayer = guild.getMember(player);

                        if (guildPlayer.getRank().isHigherOrEquals(GuildRank.LEADER)) {
                            Player target = Bukkit.getPlayer(args[1]);
                            if (target != null) {
                                if (guild.isMember(target)) {
                                    GuildPlayer targetPlayer = guild.getMember(target);

                                    if (targetPlayer.getRank().isLower(guildPlayer.getRank())) {
                                        GuildRank newRank = GuildRank.fromName(args[2]);
                                        if (newRank != null) {
                                            if (newRank.isLower(guildPlayer.getRank())) {
                                                targetPlayer.setRank(newRank, true);
                                                GuildPlugin.getInstance().getChat().sendNormal(target, Messages.Normal.YOUR_RANK_CHANGED, player.getName(), newRank.getPrettyName());
                                                GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.CHANGED_RANK, target.getName(), newRank.getPrettyName());
                                                GuildPlugin.getInstance().getPacketManager().sendPacket(new PacketUpdateRank(guild.getName(), targetPlayer.getPlayerId(), targetPlayer.getName(), newRank, player.getName()));
                                            } else {
                                                GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.GIVEN_RANK_TOO_HIGH);
                                            }
                                        } else {
                                            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.RANK_DOESNT_EXIST, args[2]);
                                        }
                                    } else {
                                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.HIGHER_RANK, target.getName());
                                    }
                                } else {
                                    GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_SAME_GUILD, target.getName());
                                }
                            } else {
                                UUID targetId = GuildPlugin.getInstance().getAsync().get(new Callable<UUID>() {
                                    @Override
                                    public UUID call() throws Exception {
                                        return GuildPlugin.getInstance().getDataBase().getIdFromName(args[1]);
                                    }
                                });

                                if (guild.isMember(targetId)) {
                                    GuildPlayer targetPlayer = guild.getMember(targetId);

                                    if (targetPlayer.getRank().isLower(guildPlayer.getRank())) {
                                        GuildRank newRank = GuildRank.fromName(args[2]);
                                        if (newRank != null) {
                                            if (newRank.isLower(guildPlayer.getRank())) {
                                                targetPlayer.setRank(newRank, true);
                                                GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.CHANGED_RANK, targetPlayer.getName(), newRank.getPrettyName());
                                                GuildPlugin.getInstance().getPacketManager().sendPacket(new PacketUpdateRank(guild.getName(), targetPlayer.getPlayerId(), targetPlayer.getName(), newRank, player.getName()));
                                            } else {
                                                GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.GIVEN_RANK_TOO_HIGH);
                                            }
                                        } else {
                                            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.RANK_DOESNT_EXIST, args[2]);
                                        }
                                    } else {
                                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.HIGHER_RANK, targetPlayer.getName());
                                    }
                                } else {
                                    GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_SAME_GUILD, args[1]);
                                }
                            }
                        } else {
                            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.GUILD_RANK, GuildRank.LEADER.getPrettyName());
                        }
                    } else {
                        GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_IN_GUILD);
                    }
                } else {
                    // TODO Show help
                }
            }
        } else {
            sendHelp(player, label);
        }
    }

    private void sendHelp(Player player, String label) {

    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Guild guild = GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player);

            if (args.length == 1) {
                return filter(args[0], "create", "leave", "delete", "invite", "accept", "setrank", "kick");
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("accept")) {
                    return filter(args[1], GuildPlugin.getInstance().getGuildManager().getInvitations(player));
                } else if (args[0].equalsIgnoreCase("setrank") || args[0].equalsIgnoreCase("kick")) {
                    if (guild != null) {
                        List<String> members = new ArrayList<>();
                        for (GuildPlayer member : guild.getMembers().values()) {
                            if (!member.getName().equals(player.getName())) {
                                members.add(member.getName());
                            }
                        }
                        return filter(args[1], members);
                    }
                }
            } else if (args.length == 3) {
                if (args[0].equalsIgnoreCase("setrank")) {
                    return filter(args[2], GuildRank.LEADER.name(), GuildRank.MEMBER.name(), GuildRank.OFFICER.name());
                }
            }
        }
        return null;
    }

    private List<String> filter(String value, String... toFilter) {
        return filter(value, Arrays.asList(toFilter));
    }

    private List<String> filter(String value, List<String> toFilter) {
        List<String> list = new ArrayList<>();

        for (String filter : toFilter) {
            if (filter.toLowerCase().startsWith(value.toLowerCase())) {
                list.add(filter);
            }
        }

        return list;
    }
}
