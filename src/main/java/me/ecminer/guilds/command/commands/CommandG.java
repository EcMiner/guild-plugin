package me.ecminer.guilds.command.commands;

import me.ecminer.guilds.GuildPlugin;
import me.ecminer.guilds.chat.Messages;
import me.ecminer.guilds.command.PlayerCommand;
import org.bukkit.entity.Player;

public class CommandG extends PlayerCommand {

    public CommandG() {
        super("g");
    }

    @Override
    public void onPlayerExecuted(Player player, String label, String[] args) {
        if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
            if (args.length > 0) {
                GuildPlugin.getInstance().getChat().sendGuildChatMessage(player, getMessage(args, 0));
            } else {
                // TODO Show help
            }
        } else {
            GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NOT_IN_GUILD);
        }
    }

    private String getMessage(String[] args, int begin) {
        StringBuilder sb = new StringBuilder();
        for (int i = begin; i < args.length; i++) {
            if (i != begin) {
                sb.append(" ");
            }
            sb.append(args[i]);
        }
        return sb.toString();
    }
}
