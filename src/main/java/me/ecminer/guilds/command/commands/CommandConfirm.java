package me.ecminer.guilds.command.commands;

import me.ecminer.guilds.GuildPlugin;
import me.ecminer.guilds.chat.Messages;
import me.ecminer.guilds.command.ConfirmAction;
import me.ecminer.guilds.command.PlayerCommand;
import org.bukkit.entity.Player;

public class CommandConfirm extends PlayerCommand {

    public CommandConfirm() {
        super("confirm");
    }

    @Override
    public void onPlayerExecuted(Player player, String label, String[] args) {
        if (args.length == 0) {
            if (ConfirmAction.hasAction(player)) {
                ConfirmAction.getAction(player).onConfirm(player);
                ConfirmAction.removeAction(player);
            } else {
                GuildPlugin.getInstance().getChat().sendError(player, Messages.Error.NO_CONFIRM_ACTION);
            }
        } else {
            // TODO Show help
        }
    }
}
