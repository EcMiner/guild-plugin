package me.ecminer.guilds.command;

import me.ecminer.guilds.GuildPlugin;
import me.ecminer.guilds.chat.Messages;
import me.ecminer.guilds.collections.ExpireHashMap;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public abstract class ConfirmAction {

    private static ExpireHashMap<UUID, ConfirmAction> confirmActions = new ExpireHashMap<UUID, ConfirmAction>();

    public static void clearAllActions(Player player) {
        confirmActions.removeKey(player.getUniqueId());
    }

    public static boolean hasAction(Player player) {
        return confirmActions.containsKey(player.getUniqueId());
    }

    public static ConfirmAction getAction(Player player) {
        return confirmActions.get(player.getUniqueId());
    }

    public static void removeAction(Player player) {
        confirmActions.removeKey(player.getUniqueId());
    }

    public ConfirmAction(Player player) {
        confirmActions.put(player.getUniqueId(), this, 10, TimeUnit.SECONDS);
        GuildPlugin.getInstance().getChat().sendNormal(player, Messages.Normal.NEEDS_CONFIRMATION);
    }

    public abstract void onConfirm(Player player);

}
