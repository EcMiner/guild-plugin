package me.ecminer.guilds.command;

import me.ecminer.guilds.GuildPlugin;
import me.ecminer.guilds.chat.Messages;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public abstract class Command implements CommandExecutor {

    private final String command;
    private String permissionNode;

    public Command(String command) {
        this.command = command;
        GuildPlugin.getInstance().getCommand(command).setExecutor(this);
    }

    public Command(String command, String permissionNode) {
        this(command);
        this.permissionNode = permissionNode;
    }

    @Override
    public final boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] strings) {
        if (permissionNode != null && !commandSender.hasPermission(permissionNode)) {
            GuildPlugin.getInstance().getChat().sendError(commandSender, Messages.Error.NO_PERMISSION);
            return false;
        }

        this.onCommandExecuted(commandSender, s, strings);
        return true;
    }

    public abstract void onCommandExecuted(CommandSender sender, String label, String[] args);

    public String getCommand() {
        return command;
    }

    public String getPermissionNode() {
        return permissionNode;
    }
}
