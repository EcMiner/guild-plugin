package me.ecminer.guilds.command;

import me.ecminer.guilds.GuildPlugin;
import me.ecminer.guilds.chat.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;

public abstract class ConsoleCommand extends Command {

    public ConsoleCommand(String command) {
        super(command);
    }

    public ConsoleCommand(String command, String permissionNode) {
        super(command, permissionNode);
    }

    @Override
    public final void onCommandExecuted(CommandSender sender, String label, String[] args) {
        if (sender instanceof ConsoleCommandSender) {
            onConsoleExecuted((ConsoleCommandSender) sender, label, args);
        } else {
            GuildPlugin.getInstance().getChat().sendError(sender, Messages.Error.CONSOLE_COMMAND);
        }
    }

    public abstract void onConsoleExecuted(ConsoleCommandSender sender, String label, String[] args);
}
