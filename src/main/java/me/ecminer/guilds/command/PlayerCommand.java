package me.ecminer.guilds.command;

import me.ecminer.guilds.GuildPlugin;
import me.ecminer.guilds.chat.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class PlayerCommand extends Command {


    public PlayerCommand(String command) {
        super(command);
    }

    public PlayerCommand(String command, String permissionNode) {
        super(command, permissionNode);
    }

    @Override
    public final void onCommandExecuted(CommandSender sender, String label, String[] args) {
        if (!(sender instanceof Player)) {
            GuildPlugin.getInstance().getChat().sendError(sender, Messages.Error.PLAYER_COMMAND);
            return;
        }
        Player player = (Player) sender;
        onPlayerExecuted(player, label, args);
    }

    public abstract void onPlayerExecuted(Player player, String label, String[] args);
}
