package me.ecminer.guilds.guild;

import me.ecminer.guilds.GuildPlugin;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.bukkit.entity.Player;

import java.util.*;

public class Guild {

    public static final String allowedCharacters = "^[a-zA-Z0-9.!#$@%&*-=+_]+";
    public static final int maxNameLength = 10;

    private final String name;
    private GuildPlayer owner;
    private final Map<UUID, GuildPlayer> members = new HashMap<>();
    private final List<String> warredGuilds = new ArrayList<>();
    private final List<String> alliedGuilds = new ArrayList<>();
    private final List<UUID> bannedPlayers = new ArrayList<>();
    private final List<UUID> onlinePlayers = new ArrayList<>();

    public Guild(String name, GuildPlayer owner) {
        this.name = name;
        this.owner = owner;
        owner.setRank(GuildRank.OWNER);
    }

    public void addOnlinePlayer(Player player) {
        addOnlinePlayer(player.getUniqueId());
    }

    public void addOnlinePlayer(UUID playerId) {
        onlinePlayers.add(playerId);
    }

    public void removeOnlinePlayer(Player player) {
        removeOnlinePlayer(player.getUniqueId());
    }

    public void removeOnlinePlayer(UUID playerId) {
        onlinePlayers.remove(playerId);
        if (getOnlinePlayerCount() == 0) {
            GuildPlugin.getInstance().getGuildManager().unloadGuild(this);
        }
    }

    public int getOnlinePlayerCount() {
        return onlinePlayers.size();
    }

    public List<GuildPlayer> getOnlinePlayers() {
        return Lists.transform(onlinePlayers, new Function<UUID, GuildPlayer>() {

            @Override
            public GuildPlayer apply(UUID uuid) {
                return members.get(uuid);
            }
        });
    }

    public GuildPlayer getOwner() {
        return owner;
    }

    public String getName() {
        return name;
    }

    public boolean isMember(Player player) {
        return isMember(player.getUniqueId());
    }

    public boolean isMember(UUID playerId) {
        return members.containsKey(playerId);
    }

    public Map<UUID, GuildPlayer> getMembers() {
        return members;
    }

    public GuildPlayer getMember(Player player) {
        return getMember(player.getUniqueId());
    }

    public GuildPlayer getMember(UUID playerId) {
        return members.get(playerId);
    }

    public void addMember(Player player, boolean updateDatabase) {
        addMember(player.getUniqueId(), player.getName(), updateDatabase);
        addOnlinePlayer(player);
    }

    public void addMember(Player player) {
        addMember(player, true);
    }

    public void addMember(UUID playerId, String name) {
        addMember(playerId, name, true);
    }

    public void addMember(UUID playerId, String name, boolean updateDatabase) {
        GuildPlayer member = new GuildPlayer(playerId, this.name).setName(name);
        members.put(playerId, member);
        GuildPlugin.getInstance().getGuildManager().setMemberInGuild(playerId, this);
        if (updateDatabase) {
            GuildPlugin.getInstance().getAsync().run(new Runnable() {

                @Override
                public void run() {
                    GuildPlugin.getInstance().getDataBase().addMember(member);
                }

            });
        }
    }

    public void removeMember(Player player, boolean updateDatabase) {
        removeMember(player.getUniqueId(), updateDatabase);
    }

    public void removeMember(Player player) {
        removeMember(player.getUniqueId());
    }

    public void removeMember(UUID playerId) {
        removeMember(playerId, true);
    }

    public void removeMember(UUID playerId, boolean updateDatabase) {
        members.remove(playerId);
        removeOnlinePlayer(playerId);
        GuildPlugin.getInstance().getGuildManager().removeMemberInGuild(playerId);
        if (getMemberCount() == 0) {
            GuildPlugin.getInstance().getGuildManager().deleteGuild(this);
        } else {
            if (updateDatabase) {
                GuildPlugin.getInstance().getAsync().run(new Runnable() {

                    @Override
                    public void run() {
                        GuildPlugin.getInstance().getDataBase().removeMember(playerId);
                    }

                });
            }
        }
    }

    public void banPlayer(Player player) {
        banPlayer(player.getUniqueId());
    }

    public void banPlayer(UUID playerId) {
        bannedPlayers.add(playerId);
        if (members.containsKey(playerId)) {
            removeMember(playerId);
        }
        GuildPlugin.getInstance().getAsync().run(new Runnable() {

            @Override
            public void run() {
                GuildPlugin.getInstance().getDataBase().banPlayer(Guild.this, playerId);
            }

        });
    }

    public List<String> getWarredGuilds() {
        return warredGuilds;
    }

    public List<String> getAlliedGuilds() {
        return alliedGuilds;
    }

    public List<UUID> getBannedPlayers() {
        return bannedPlayers;
    }

    public void addWarredGuild(Guild guild) {
        addWarredGuild(guild.getName());
    }

    public void addWarredGuild(String guildName) {
        if (!guildName.equalsIgnoreCase(name)) {
            warredGuilds.add(guildName.toLowerCase());
            GuildPlugin.getInstance().getAsync().run(new Runnable() {

                @Override
                public void run() {
                    GuildPlugin.getInstance().getDataBase().addWarredGuild(Guild.this, guildName);
                }

            });
        }
    }

    public void removeWarredGuild(Guild guild) {
        removeWarredGuild(guild.getName());
    }

    public void removeWarredGuild(String guildName) {
        if (warredGuilds.contains(guildName.toLowerCase())) {
            warredGuilds.remove(guildName.toLowerCase());
            GuildPlugin.getInstance().getAsync().run(new Runnable() {

                @Override
                public void run() {
                    GuildPlugin.getInstance().getDataBase().removeWarredGuild(Guild.this, guildName);
                }

            });
        }
    }

    public void removeAlliedGuild(Guild guild) {
        removeAlliedGuild(guild.getName());
    }

    public void removeAlliedGuild(String guildName) {
        if (alliedGuilds.contains(guildName.toLowerCase())) {
            alliedGuilds.remove(guildName.toLowerCase());
            GuildPlugin.getInstance().getAsync().run(new Runnable() {

                @Override
                public void run() {
                    GuildPlugin.getInstance().getDataBase().removeAlliedGuild(Guild.this, guildName);
                }

            });
        }
    }

    public void addAlliedGuild(Guild guild) {
        addAlliedGuild(guild.getName());
    }

    public void addAlliedGuild(String guildName) {
        if (!guildName.equalsIgnoreCase(name)) {
            alliedGuilds.add(guildName.toLowerCase());
            GuildPlugin.getInstance().getAsync().run(new Runnable() {

                @Override
                public void run() {
                    GuildPlugin.getInstance().getDataBase().addAlliedGuild(Guild.this, guildName);
                }

            });
        }
    }

    public void setOwner(Player player) {
        setOwner(player.getUniqueId());
    }

    public void setOwner(Player player, boolean updateDatabase) {
        setOwner(player.getUniqueId(), updateDatabase);
    }

    public void setOwner(UUID playerId) {
        setOwner(playerId, true);
    }

    public void setOwner(UUID playerId, boolean updateDatabase) {
        if (!members.containsKey(playerId) && playerId != owner.getPlayerId()) {
            this.owner = getMember(playerId);

            if (updateDatabase) {
                GuildPlugin.getInstance().getAsync().run(new Runnable() {

                    @Override
                    public void run() {
                        GuildPlugin.getInstance().getDataBase().setOwner(Guild.this, playerId);
                    }

                });
            }
        }
    }

    public int getMemberCount() {
        return members.size();
    }
}
