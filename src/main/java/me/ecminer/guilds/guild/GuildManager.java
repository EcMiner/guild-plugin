package me.ecminer.guilds.guild;

import me.ecminer.guilds.GuildPlugin;
import me.ecminer.guilds.collections.CaseInsensitiveMap;
import me.ecminer.guilds.collections.ExpireArrayList;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class GuildManager {

    private Map<String, Guild> loadedGuilds = new CaseInsensitiveMap<>();
    private Map<UUID, Guild> membersInGuild = new HashMap<>();
    private Map<UUID, ExpireArrayList<String>> invitations = new HashMap<>();

    public boolean isGuildLoaded(String guildName) {
        return loadedGuilds.containsKey(guildName);
    }

    public Guild getGuild(String guildName) {
        return loadedGuilds.get(guildName);
    }

    public void setMemberInGuild(Player player, Guild guild) {
        setMemberInGuild(player.getUniqueId(), guild);
    }

    public void setMemberInGuild(UUID playerId, Guild guild) {
        membersInGuild.put(playerId, guild);
    }

    public void removeMemberInGuild(Player player) {
        removeMemberInGuild(player.getUniqueId());
    }

    public void removeMemberInGuild(UUID playerId) {
        membersInGuild.remove(playerId);
    }

    public boolean isPlayerInGuild(Player player) {
        return isPlayerInGuild(player.getUniqueId());
    }

    public boolean isPlayerInGuild(UUID playerId) {
        return membersInGuild.containsKey(playerId);
    }

    public Guild getGuildFromMember(Player player) {
        return getGuildFromMember(player.getUniqueId());
    }

    public Guild getGuildFromMember(UUID playerId) {
        return membersInGuild.get(playerId);
    }

    public void unloadGuild(Guild guild) {
        for (UUID member : guild.getMembers().keySet()) {
            removeMemberInGuild(member);
        }
        loadedGuilds.remove(guild.getName());
    }

    public int getAmountOfLoadedGuilds() {
        return loadedGuilds.size();
    }

    public Guild loadGuild(String guildName, boolean sync) {
        Guild guild;
        if (sync) {
            long start = System.currentTimeMillis();
            guild = GuildPlugin.getInstance().getDataBase().loadGuild(guildName);
            long end = System.currentTimeMillis();
            System.out.println("Loading took: " + (end - start) + "ms");
        } else {
            guild = GuildPlugin.getInstance().getAsync().get(new Callable<Guild>() {

                @Override
                public Guild call() throws Exception {
                    return GuildPlugin.getInstance().getDataBase().loadGuild(guildName);
                }

            });
        }
        if (guild != null) {
            for (UUID member : guild.getMembers().keySet()) {
                setMemberInGuild(member, guild);
            }
            loadedGuilds.put(guildName, guild);
        }
        return guild;
    }

    public Guild createGuild(String guildName, Player owner) {
        if (!isGuildLoaded(guildName) && !isPlayerInGuild(owner)) {
            GuildPlayer player = new GuildPlayer(owner, guildName).setName(owner.getName());

            Guild guild = new Guild(guildName, player);
            guild.addOnlinePlayer(owner.getUniqueId());
            guild.getMembers().put(owner.getUniqueId(), player);
            setMemberInGuild(owner, guild);

            GuildPlugin.getInstance().getAsync().run(new Runnable() {

                @Override
                public void run() {
                    GuildPlugin.getInstance().getDataBase().createGuild(guild);
                }

            });

            return guild;
        }
        return null;
    }

    public void deleteGuild(Guild guild) {
        unloadGuild(guild);
        GuildPlugin.getInstance().getAsync().run(new Runnable() {

            @Override
            public void run() {
                GuildPlugin.getInstance().getDataBase().deleteGuild(guild);
            }

        });
    }

    public void addInvitation(Player player, Guild guild) {
        if (!invitations.containsKey(player.getUniqueId())) {
            invitations.put(player.getUniqueId(), new ExpireArrayList<>());
        }
        invitations.get(player.getUniqueId()).add(guild.getName().toLowerCase(), 20, TimeUnit.SECONDS);
    }

    public boolean hasInvitations(Player player) {
        return invitations.containsKey(player.getUniqueId()) && invitations.get(player.getUniqueId()).size() > 0;
    }

    public boolean hasInvitation(Player player, String guildName) {
        return invitations.containsKey(player.getUniqueId()) && invitations.get(player.getUniqueId()).contains(guildName.toLowerCase());
    }

    public List<String> getInvitations(Player player) {
        List<String> list = new ArrayList<>();

        if (invitations.containsKey(player.getUniqueId())) {
            invitations.get(player.getUniqueId()).contains("");
            for (String string : invitations.get(player.getUniqueId())) {
                list.add(string);
            }
        }

        return list;
    }

    public void removeInvitations(Player player) {
        invitations.remove(player.getUniqueId());
    }

    public void unload() {
        loadedGuilds.clear();
        loadedGuilds = null;

        membersInGuild.clear();
        membersInGuild = null;
    }

}
