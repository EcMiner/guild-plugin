package me.ecminer.guilds.guild;

public enum GuildRank {

    OWNER(3, "Owner"), LEADER(2, "Leader"), OFFICER(1, "Officer"), MEMBER(0, "Member");

    public static GuildRank fromName(String name) {
        for (GuildRank rank : values()) {
            if (rank.name().equalsIgnoreCase(name) || rank.getPrettyName().equalsIgnoreCase(name)) {
                return rank;
            }
        }
        return null;
    }

    private final int rankLadder;
    private final String prettyName;

    GuildRank(int rankLadder, String prettyName) {
        this.rankLadder = rankLadder;
        this.prettyName = prettyName;
    }

    public int getRankLadder() {
        return rankLadder;
    }

    public boolean isHigherOrEquals(GuildRank rank) {
        return this.rankLadder >= rank.rankLadder;
    }

    public boolean isHigher(GuildRank rank) {
        return this.rankLadder > rank.rankLadder;
    }

    public boolean isLowerOrEquals(GuildRank rank) {
        return this.rankLadder <= rank.rankLadder;
    }

    public boolean isLower(GuildRank rank) {
        return this.rankLadder < rank.rankLadder;
    }

    public String getPrettyName() {
        return prettyName;
    }
}
