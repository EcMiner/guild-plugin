package me.ecminer.guilds.guild;

import me.ecminer.guilds.GuildPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class GuildPlayer {

    private final UUID playerId;
    private final String guildName;
    private GuildRank rank = GuildRank.MEMBER;
    private String name;

    public GuildPlayer(Player player, String guildName) {
        this(player.getUniqueId(), guildName);
    }

    public GuildPlayer(UUID playerId, String guildName) {
        this.playerId = playerId;
        this.guildName = guildName;
    }

    public GuildPlayer setName(String name) {
        this.name = name;
        return this;
    }

    public String getName() {
        return name;
    }

    public String getGuildName() {
        return guildName;
    }

    public UUID getPlayerId() {
        return playerId;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(playerId);
    }

    public boolean isOnline() {
        return getPlayer() != null;
    }

    public void setRank(GuildRank rank) {
        setRank(rank, true);
    }

    public void setRank(GuildRank rank, boolean updateDatabase) {
        this.rank = rank;
        if (updateDatabase) {
            GuildPlugin.getInstance().getAsync().run(new Runnable() {
                @Override
                public void run() {
                    GuildPlugin.getInstance().getDataBase().setRank(playerId, rank);
                }
            });
        }
    }

    public GuildRank getRank() {
        return rank;
    }
}
