package me.ecminer.guilds.collections;

import java.util.HashMap;

public class CaseInsensitiveMap<V> extends HashMap<String, V> {

    @Override
    public V put(String key, V value) {
        return super.put(key.toLowerCase(), value);
    }

    @Override
    public boolean containsKey(Object key) {
        return key instanceof String && super.containsKey(((String) key).toLowerCase());
    }

    @Override
    public V get(Object key) {
        return key instanceof String ? super.get(((String) key).toLowerCase()) : null;
    }

    @Override
    public V getOrDefault(Object key, V defaultValue) {
        return key instanceof String ? super.getOrDefault(((String) key).toLowerCase(), defaultValue) : defaultValue;
    }

    @Override
    public V remove(Object key) {
        return key instanceof String ? super.remove(((String) key).toLowerCase()) : null;
    }

    @Override
    public boolean remove(Object key, Object value) {
        return key instanceof String && super.remove(((String) key).toLowerCase(), value);
    }
}
