package me.ecminer.guilds.collections;

import com.google.common.base.Preconditions;
import com.google.common.base.Ticker;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.TimeUnit;

public class ExpireArrayList<T> implements Iterable<T> {

    private Ticker ticker;

    public ExpireArrayList(Ticker ticker) {
        this.ticker = ticker;
    }

    public ExpireArrayList() {
        this(Ticker.systemTicker());
    }

    private class ExpireEntry implements Comparable<ExpireEntry> {

        public final long expireTime;
        public final T expireObject;

        public ExpireEntry(long expireTime, T expireObject) {
            this.expireTime = expireTime;
            this.expireObject = expireObject;
        }

        @Override
        public int compareTo(ExpireEntry o) {
            return Long.compare(expireTime, o.expireTime);
        }

        @Override
        public String toString() {
            return "ExpireEntry [expireTime=" + expireTime + ", expireObject=" + expireObject + "]";
        }

    }

    private List<T> list = new ArrayList<T>();
    private PriorityQueue<ExpireEntry> expireQueue = new PriorityQueue<ExpireEntry>();

    public T get(int index) {
        return list.get(index);
    }

    public T add(T object, long expireDelay, TimeUnit expireUnit) {
        Preconditions.checkNotNull(object, "expireUnit cannot be NULL");
        Preconditions.checkState(expireDelay > 0, "expireDelay cannot be equal or less than zero.");
        evictExpired();

        ExpireEntry entry = new ExpireEntry(ticker.read() + TimeUnit.NANOSECONDS.convert(expireDelay, expireUnit), object);
        list.add(object);
        expireQueue.add(entry);
        return object;
    }

    public boolean contains(T object) {
        evictExpired();
        return list.contains(object);
    }

    public T remove(T object) {
        evictExpired();
        list.remove(object);
        return object;
    }

    public int size() {
        evictExpired();
        return list.size();
    }

    @Override
    public Iterator<T> iterator() {
        return list.iterator();
    }

    public void clear() {
        list.clear();
        expireQueue.clear();
    }

    protected void evictExpired() {
        long currentTime = ticker.read();
        while (expireQueue.size() > 0 && expireQueue.peek().expireTime <= currentTime) {
            ExpireEntry entry = expireQueue.poll();
            list.remove(entry.expireObject);
        }
    }
}