package me.ecminer.guilds.listener;

import me.ecminer.guilds.GuildPlugin;
import me.ecminer.guilds.command.ConfirmAction;
import me.ecminer.guilds.guild.Guild;
import me.ecminer.guilds.guild.GuildPlayer;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)

    public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent evt) {
        if (evt.getLoginResult() == AsyncPlayerPreLoginEvent.Result.ALLOWED) {
            if (!GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(evt.getUniqueId())) {
                String guildName = GuildPlugin.getInstance().getDataBase().getPlayerGuild(evt.getUniqueId());

                if (guildName != null) {
                    if (GuildPlugin.getInstance().getGuildManager().isGuildLoaded(guildName)) {
                        GuildPlugin.getInstance().getGuildManager().setMemberInGuild(evt.getUniqueId(), GuildPlugin.getInstance().getGuildManager().getGuild(guildName));
                    } else {
                        GuildPlugin.getInstance().getGuildManager().loadGuild(guildName, true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent evt) {
        Player player = evt.getPlayer();
        if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
            GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player).addOnlinePlayer(player);

            Guild guild = GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player);
            GuildPlayer guildPlayer = guild.getMember(player);
            if (!guildPlayer.getName().equals(player.getName())) {
                GuildPlugin.getInstance().getAsync().run(new Runnable() {
                    @Override
                    public void run() {
                        GuildPlugin.getInstance().getDataBase().updateName(player.getUniqueId(), player.getName());
                    }
                });
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent evt) {
        Player player = evt.getPlayer();
        ConfirmAction.clearAllActions(player);
        GuildPlugin.getInstance().getGuildManager().removeInvitations(player);
        if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
            GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player).removeOnlinePlayer(player);
        }
    }

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent evt) {
        Player player = evt.getPlayer();
        if (GuildPlugin.getInstance().getGuildManager().isPlayerInGuild(player)) {
            evt.setFormat(ChatColor.GRAY + "[" + ChatColor.GREEN + GuildPlugin.getInstance().getGuildManager().getGuildFromMember(player).getName() + ChatColor.GRAY + "] " + evt.getFormat());
        }
    }

}
